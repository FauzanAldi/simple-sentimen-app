package config

import (
	"encoding/base64"
	"io"
	"mime/multipart"
)

func ConvertToBase64(file *multipart.FileHeader) (string, error) {
	// Open the file
	fileContent, err := file.Open()
	if err != nil {
		return "", err
	}
	defer fileContent.Close()

	// Read the file content
	fileBytes, err := io.ReadAll(fileContent)
	if err != nil {
		return "", err
	}

	// Encode file to base64
	base64Data := base64.StdEncoding.EncodeToString(fileBytes)
	return base64Data, nil
}
