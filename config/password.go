package config

import (
	"strings"

	"github.com/go-playground/validator"
)

func ValidatePassword(fl validator.FieldLevel) bool {
	password := fl.Field().String()

	if len(password) < 8 {
		return false
	}

	// Minimal satu huruf besar, satu huruf kecil, satu angka, dan satu karakter khusus
	hasUppercase := containsUppercase(password)
	hasLowercase := containsLowercase(password)
	hasDigit := containsDigit(password)
	hasSymbol := containsSymbol(password)

	return hasUppercase && hasLowercase && hasDigit && hasSymbol
}

func containsUppercase(s string) bool {
	return strings.ContainsAny(s, "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
}

func containsLowercase(s string) bool {
	return strings.ContainsAny(s, "abcdefghijklmnopqrstuvwxyz")
}

func containsDigit(s string) bool {
	return strings.ContainsAny(s, "0123456789")
}

func containsSymbol(s string) bool {
	symbols := "~`!@#$%^&*()-_+={}[]|;:'<>,.?/"
	return strings.ContainsAny(s, symbols)
}
