package config

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
)

func GenerateToken(username string, user_id uint, expireAt time.Time) string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username": username,
		"user_id":  user_id,
		"exp":      expireAt.Unix(),
	})

	tokenString, err := token.SignedString([]byte(GoDotEnvVariable("SECRET_KEY")))
	if err != nil {
		panic("Failed to generate token")
	}

	return tokenString
}

func GenerateTokenRefresh(username string, user_id uint, expireAt time.Time) string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username": username,
		"user_id":  user_id,
		"exp":      expireAt.Unix(),
	})

	tokenString, err := token.SignedString([]byte(GoDotEnvVariable("SECRET_KEY_REFRESH_TOKEN")))
	if err != nil {
		panic("Failed to generate token")
	}

	return tokenString
}

func AccessTokenExpired() time.Time {
	data := GoDotEnvVariable("ACCESS_TOKEN_EXPIRE")

	// Konversi string ke int
	durationValue := StrToInt(data, 1)

	// Konversi int ke time.Duration
	duration := time.Hour * time.Duration(durationValue)

	// Tambahkan duration ke waktu sekarang
	result := time.Now().Add(duration)

	return result
}

func RefreshTokenExpired() time.Time {
	data := GoDotEnvVariable("REFRESH_TOKEN_EXPIRE")

	// Konversi string ke int
	durationValue := StrToInt(data, 168)

	// Konversi int ke time.Duration
	duration := time.Hour * time.Duration(durationValue)

	// Tambahkan duration ke waktu sekarang
	result := time.Now().Add(duration)

	return result
}

func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		tokenString := c.GetHeader("Authorization")
		tokenString = strings.TrimPrefix(tokenString, "Bearer ")

		if tokenString == "" {
			ErrorResponse(c, http.StatusUnauthorized, "Unauthorized")
			c.Abort()
			return
		}

		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			return []byte(GoDotEnvVariable("SECRET_KEY")), nil
		})

		if err != nil || !token.Valid {
			fmt.Println(err)
			ErrorResponse(c, http.StatusUnauthorized, "Unauthorized")
			c.Abort()
			return
		}

		claims, _ := token.Claims.(jwt.MapClaims)
		c.Set("userID", claims["user_id"])
		c.Set("email", claims["username"])

		userString := fmt.Sprintf("%v", claims["user_id"])
		userId := StrToInt(userString, 0)

		SaveLog(c, userId)
		c.Next()
	}
}

func AuthMiddlewareRefreshToken() gin.HandlerFunc {
	return func(c *gin.Context) {
		tokenString := c.GetHeader("Authorization")
		tokenString = strings.TrimPrefix(tokenString, "Bearer ")

		if tokenString == "" {
			ErrorResponse(c, http.StatusUnauthorized, "Unauthorized")
			c.Abort()
			return
		}

		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			return []byte(GoDotEnvVariable("SECRET_KEY_REFRESH_TOKEN")), nil
		})

		if err != nil || !token.Valid {
			fmt.Println(err)
			ErrorResponse(c, http.StatusUnauthorized, "Unauthorized")
			c.Abort()
			return
		}

		claims, _ := token.Claims.(jwt.MapClaims)
		c.Set("userID", claims["user_id"])
		c.Set("email", claims["username"])

		userString := fmt.Sprintf("%v", claims["user_id"])
		userId := StrToInt(userString, 0)
		SaveLog(c, userId)

		c.Next()
	}
}

func AuthMiddlewareOptional() gin.HandlerFunc {
	return func(c *gin.Context) {
		tokenString := c.GetHeader("Authorization")
		tokenString = strings.TrimPrefix(tokenString, "Bearer ")
		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			return []byte(GoDotEnvVariable("SECRET_KEY")), nil
		})

		if err != nil || !token.Valid {
			c.Set("userID", nil)
			c.Set("email", nil)

			SaveLog(c, 0)

		} else {

			claims, _ := token.Claims.(jwt.MapClaims)
			c.Set("userID", claims["user_id"])
			c.Set("email", claims["username"])

			userString := fmt.Sprintf("%v", claims["user_id"])
			userId := StrToInt(userString, 0)
			SaveLog(c, userId)

		}

		c.Next()
	}
}
