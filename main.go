package main

import (
	"log"
	"simple-sentiment-app/golang/config"
	"simple-sentiment-app/golang/routes"
	"time"

	"github.com/joho/godotenv"
)

func main() {

	db := config.ConnectDataBase()
	sqlDB, _ := db.DB()
	defer sqlDB.Close()

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	loc, _ := time.LoadLocation("Asia/Jakarta")
	time.Local = loc

	r := routes.SetupRouter(db)
	r.Run(":" + config.GoDotEnvVariable("APP_PORT"))
}
