package config

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"fmt"
)

func Encrypt(plainText string) string {
	block, err := aes.NewCipher([]byte(GoDotEnvVariable("IDENTITY_NUMBER_KEY")))
	if err != nil {
		fmt.Println(err)
		return ""
	}

	// Generate random IV
	iv := make([]byte, aes.BlockSize)
	if _, err := rand.Read(iv); err != nil {
		fmt.Println(err)
		return ""
	}

	// Pad the plaintext to be a multiple of the block size
	plainTextBytes := []byte(plainText)
	padding := aes.BlockSize - len(plainTextBytes)%aes.BlockSize
	padText := append(plainTextBytes, bytes.Repeat([]byte{byte(padding)}, padding)...)

	// Encrypt the padded plaintext
	cipherText := make([]byte, aes.BlockSize+len(padText))
	mode := cipher.NewCBCEncrypter(block, iv)
	mode.CryptBlocks(cipherText[aes.BlockSize:], padText)

	// Prepend the IV to the ciphertext
	copy(cipherText[:aes.BlockSize], iv)

	// Encode the result in base64 for easy printing
	return base64.StdEncoding.EncodeToString(cipherText)
}

func Decrypt(cipherText string) string {
	cipherTextBytes, err := base64.StdEncoding.DecodeString(cipherText)
	if err != nil {
		return ""
	}

	block, err := aes.NewCipher([]byte(GoDotEnvVariable("IDENTITY_NUMBER_KEY")))
	if err != nil {
		return ""
	}

	// Extract IV from the ciphertext
	iv := cipherTextBytes[:aes.BlockSize]
	cipherTextBytes = cipherTextBytes[aes.BlockSize:]

	// Decrypt the ciphertext
	mode := cipher.NewCBCDecrypter(block, iv)
	mode.CryptBlocks(cipherTextBytes, cipherTextBytes)

	// Remove padding
	padding := int(cipherTextBytes[len(cipherTextBytes)-1])
	plainText := cipherTextBytes[:len(cipherTextBytes)-padding]

	return string(plainText)
}
