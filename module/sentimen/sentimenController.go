package sentimen

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/RadhiFadlillah/go-sastrawi"
	"github.com/gin-gonic/gin"
)

type SentimentResult struct {
	Review    string `json:"review"`
	Sentiment string `json:"sentiment"`
}

func AnalyzeSentimentHandler(c *gin.Context) {
	var request struct {
		Reviews []string `json:"reviews" binding:"required"`
	}

	if err := c.BindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var results []SentimentResult

	for _, review := range request.Reviews {
		// Tokenizing
		tokens := strings.Fields(review)
		fmt.Println("Token:", tokens)

		// Filtering
		stopWords := []string{
			"dan", "di", "itu", "saya", "kamu", "dia", "kita", "mereka", "juga", "akan",
			"dari", "dengan", "ke", "oleh", "pada", "untuk", "adalah", "sebagai", "menjadi",
			"atau", "namun", "tetapi", "itulah", "ini", "saat", "ketika", "sekarang", "bagi",
			"hanya", "yang", "trims", "udah", "gan", "makasih", "terima", "kasih", "ternyata",
			"mantap", "mudah2an", "awet", "bagus", "cepat", "ok", "barang", "ny", "gagal",
			"tidak", "recommended", "sepatu", "ga", "cocok", "bahan", "kanan", "kiri", "berbeda",
			"ngembun", "datang", "baik", "bungkus", "kualitas", "belum", "dicoba", "awet", "rusak",
			"keterima", "kondisi", "sabtu", "satu", "terima", "sis", "tp", "krg", "rapih", "ny",
			"nyamuk", "ny", "ny", "ny", "ny", "ny", "klem", "nempel", "ngelupas", "ketok2", "palu",
			"lagi", "gak", "cocok", "staples", "sekali", "pake", "brng", "sy", "pesan", "uang", "sy",
			"transfr", "tp", "brmg", "tdk", "sy", "trima", "pesan", "wahana", "cargo", "tdk", "ad",
			"solusi", "dr", "toped", "ut", "mngembalikan", "uang", "saya", "tdk", "sesuai", "permintaan",
			"warnanya", "di", "tanya", "ngaku2nya", "udh", "chat", "kla", "ga", "ada", "chat", "suckkkk",
			"kecewa", "lama", "sayang", "mengecewakan", "kecil", "banget", "gagal", "tidak", "recommended",
			"ga", "cocok", "rusak", "kecewa", "tidak", "sesuai", "kotor", "kurang", "rapih", "tidak", "baik",
			"lama", "tidak", "sesuai", "ga", "cocok", "rusak", "kecewa", "rusak", "tidak", "sesuai", "tidak",
			"baik", "kecewa", "buruk", "lama", "lagi", "tidak", "baik", "kecewa", "buruk", "tidak", "baik",
			"rusak", "lama", "buruk", "kecewa", "rusak", "buruk", "tidak", "baik", "kecewa", "buruk", "lama",
			"lagi", "baik",
		}
		filteredTokens := filterTokens(tokens, stopWords)
		fmt.Println("Filtering:", filteredTokens)

		// Stemming
		stemmedTokens := stemTokens(filteredTokens)
		fmt.Println("Stemmed Tokens:", stemmedTokens)

		// Sentiment Analysis
		sentiment := analyzeSentiment(stemmedTokens)

		// Menyimpan hasil sentimen dan review
		result := SentimentResult{
			Review:    review,
			Sentiment: sentiment,
		}
		results = append(results, result)
	}

	c.JSON(http.StatusOK, gin.H{"results": results})
}

func filterTokens(tokens []string, stopWords []string) []string {
	var filteredTokens []string
	for _, token := range tokens {
		if !contains(stopWords, token) {
			filteredTokens = append(filteredTokens, token)
		}
	}
	return filteredTokens
}

func stemTokens(tokens []string) []string {

	dictionary := sastrawi.DefaultDictionary()
	stemmer := sastrawi.NewStemmer(dictionary)
	var stemmedTokens []string
	for _, token := range tokens {
		stemmedTokens = append(stemmedTokens, stemmer.Stem(token))
	}
	return stemmedTokens
}

func analyzeSentiment(tokens []string) string {
	// Positive Words
	positiveWords := []string{
		"trims", "udah", "gan", "makasih", "terima", "kasih", "ternyata", "mantap", "mudah2an",
		"awet", "bagus", "cepat", "ok", "barang", "oke", "sesuai", "paket", "rapi", "mantap", "puas",
		"satisfying", "top", "super", "josss", "pokok", "bisa", "buat", "langganan", "berfungsi", "baik",
		"normal",
	}

	// Negative Words
	negativeWords := []string{
		"sedikit", "kecewa", "lama", "sayang", "mengecewakan", "kecil", "banget", "gagal", "tidak",
		"recommended", "ga", "cocok", "rusak", "kecewa", "tidak", "sesuai", "kotor", "kurang", "rapih",
		"tidak", "baik", "lama", "tidak", "sesuai", "ga", "cocok", "rusak", "kecewa", "rusak", "tidak",
		"sesuai", "tidak", "baik", "kecewa", "buruk", "lama", "lagi", "tidak", "baik", "kecewa", "buruk",
		"tidak", "baik", "rusak", "lama", "buruk", "kecewa", "rusak", "buruk", "tidak", "baik", "kecewa",
		"buruk", "lama", "lagi", "baik",
	}

	positiveCount := 0
	negativeCount := 0

	for _, token := range tokens {
		if contains(positiveWords, token) {
			positiveCount++
		} else if contains(negativeWords, token) {
			negativeCount++
		}
	}

	// Penentuan sentimen berdasarkan jumlah kata positif dan negatif
	if positiveCount > negativeCount {
		return "positif"
	} else if negativeCount > positiveCount {
		return "negatif"
	} else {
		return "netral"
	}
}

func contains(slice []string, value string) bool {
	for _, item := range slice {
		if item == value {
			return true
		}
	}
	return false
}
