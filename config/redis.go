package config

import (
	"context"
	"fmt"
	"time"

	"github.com/redis/go-redis/v9"
)

var RedisClient *redis.Client

// InitializeRedis initializes the Redis client
func InitializeRedis() {
	RedisClient = redis.NewClient(&redis.Options{
		Addr:     GoDotEnvVariable("REDIS_HOST"),     // Update with your Redis server address
		Password: GoDotEnvVariable("REDIS_PASSWORD"), // Add your Redis password if applicable
		DB:       0,                                  // Use default DB
	})

	// Ping the Redis server to check if the connection is successful
	if _, err := RedisClient.Ping(context.Background()).Result(); err != nil {
		fmt.Println("Failed to connect to Redis:", err)
		return
	}

	fmt.Println("Connected to Redis")
}

// GetRedisClient returns the Redis client for reuse
func GetRedisClient() *redis.Client {
	return RedisClient
}

// SetWithExpiration sets a key-value pair in Redis with expiration time
func SetWithExpiration(ctx context.Context, key string, value interface{}, expiration time.Duration) error {
	return RedisClient.Set(ctx, key, value, expiration).Err()
}

// Get retrieves a value from Redis based on the key
func GetRedis(ctx context.Context, key string) (string, error) {
	return RedisClient.Get(ctx, key).Result()
}
