package config

import (
	"fmt"
	"regexp"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator"
)

var validate *validator.Validate

func ValidateName(fl validator.FieldLevel) bool {
	name := fl.Field().String()

	// Hanya memperbolehkan huruf dan spasi
	matched, _ := regexp.MatchString("^[a-zA-Z ]+$", name)
	return matched
}

func LoadInputValidation(c *gin.Context, input interface{}) bool {
	// Inisialisasi validator
	validate = validator.New()

	validate.RegisterValidation("alphaspaces", ValidateName)
	validate.RegisterValidation("customPassword", ValidatePassword)

	// Validasi menggunakan validator
	if err := validate.Struct(input); err != nil {
		errorResponse := make(map[string]string)

		validationErrors := err.(validator.ValidationErrors)
		for _, e := range validationErrors {
			fieldName := e.Field()
			errorResponse[fieldName] = CustomMessage(e.Field())
		}

		fmt.Println(err)

		ErrorResponse(c, 400, "Validasi Gagal", errorResponse)

		return true

	}

	return false
}

func CustomMessage(data string) string {
	var result string
	switch data {
	case "Name":
		result = "Wajib diisi, hanya boleh berisi huruf dan spasi"
	case "Email":
		result = "Wajib diisi, harus merupakan alamat email yang valid"
	case "IdentityNumber":
		result = "Wajib berisi angka dengan panjang 16 digit"
	case "Password":
		result = "Wajib diisi, minimal 8 karakter dengan kombinasi huruf kapital, huruf kecil, angka, dan simbol"
	case "ConfirmPassword":
		result = "Wajib diisi, harus sama dengan password"
	case "ActivationCode":
		result = "Wajib diisi"
	case "PhoneNumber":
		result = "Wajib diisi, hanya boleh berisi angka"
	case "UserID":
		result = "Wajib diisi"
	case "FCMToken":
		result = "Wajib diisi"
	case "WhatsappNumber":
		result = "Wajib diisi, hanya boleh berisi angka"
	default:
		result = "Wajib diisi"
	}
	return result
}
