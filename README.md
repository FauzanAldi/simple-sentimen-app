# Simple Sentiment App

## Team
1. Mohamad Fauzan Aldi 	    (2401997570)
2. Farisa Mukti Arta Mevia	(2402012513)
3. Viola Salvadora 		    (2402012500)
4. Aris Berly Anggara		(2402012532)
5. Sheena Abigail		    (2401999821)

## Overview

Simple Sentiment App adalah aplikasi Berbasis Golang untuk analisis sentimen sederhana. Ini menggunakan metode preprocessing dan analisan sentimen sederhana.
Tahapan preprocessing dan analisan sentimen meliputi:

1. Tokenizing
Operasi pemisahan string input berdasarkan tiap kata yang menyusunya.
```go
    // Tokenizing
    tokens := strings.Fields(review)
```

2. Filtering
Mengumpulkan kata penting yang telah diurai tiap kata dari sebuah kalimat. Pada tahap ini dilakukan pembersihan kata-kata yang dianggap tidak penting seperti tanda baca dan lainnya
```go
    // Handler
    stopWords := []string{
        "dan", "di", "itu", "saya", "kamu", "dia", "kita", "mereka", "juga", "akan",
        "dari", "dengan", "ke", "oleh", "pada", "untuk", "adalah", "sebagai", "menjadi",
        "atau", "namun", "tetapi", "itulah", "ini", "saat", "ketika", "sekarang", "bagi",
        "hanya", "yang", "trims", "udah", "gan", "makasih", "terima", "kasih", "ternyata",
        "mantap", "mudah2an", "awet", "bagus", "cepat", "ok", "barang", "ny", "gagal",
        "tidak", "recommended", "sepatu", "ga", "cocok", "bahan", "kanan", "kiri", "berbeda",
        "ngembun", "datang", "baik", "bungkus", "kualitas", "belum", "dicoba", "awet", "rusak",
        "keterima", "kondisi", "sabtu", "satu", "terima", "sis", "tp", "krg", "rapih", "ny",
        "nyamuk", "ny", "ny", "ny", "ny", "ny", "klem", "nempel", "ngelupas", "ketok2", "palu",
        "lagi", "gak", "cocok", "staples", "sekali", "pake", "brng", "sy", "pesan", "uang", "sy",
        "transfr", "tp", "brmg", "tdk", "sy", "trima", "pesan", "wahana", "cargo", "tdk", "ad",
        "solusi", "dr", "toped", "ut", "mngembalikan", "uang", "saya", "tdk", "sesuai", "permintaan",
        "warnanya", "di", "tanya", "ngaku2nya", "udh", "chat", "kla", "ga", "ada", "chat", "suckkkk",
        "kecewa", "lama", "sayang", "mengecewakan", "kecil", "banget", "gagal", "tidak", "recommended",
        "ga", "cocok", "rusak", "kecewa", "tidak", "sesuai", "kotor", "kurang", "rapih", "tidak", "baik",
        "lama", "tidak", "sesuai", "ga", "cocok", "rusak", "kecewa", "rusak", "tidak", "sesuai", "tidak",
        "baik", "kecewa", "buruk", "lama", "lagi", "tidak", "baik", "kecewa", "buruk", "tidak", "baik",
        "rusak", "lama", "buruk", "kecewa", "rusak", "buruk", "tidak", "baik", "kecewa", "buruk", "lama",
        "lagi", "baik",
    }

    filteredTokens := filterTokens(tokens, stopWords)

    // filterTokens Func
    func filterTokens(tokens []string, stopWords []string) []string {
        var filteredTokens []string
        for _, token := range tokens {
            if !contains(stopWords, token) {
                filteredTokens = append(filteredTokens, token)
            }
        }
        return filteredTokens
    }
```

3. Stemming
Cara untuk penataan dan pemaparan berbagai bentuk (variants) dari suatu kata menjadi bentuk kata dasarnya (stem).

Untuk Stemming Kita Menggunakan Bantuan Library https://github.com/RadhiFadlillah/go-sastrawi

```go
    import (
        "fmt"
        "net/http"
        "strings"

        "github.com/RadhiFadlillah/go-sastrawi"
        "github.com/gin-gonic/gin"
    )

    // Stemming Handler
    stemmedTokens := stemTokens(filteredTokens)
    fmt.Println("Stemmed Tokens:", stemmedTokens)

    // Stemmer Func
    func stemTokens(tokens []string) []string {
        dictionary := sastrawi.DefaultDictionary()
        stemmer := sastrawi.NewStemmer(dictionary)
        var stemmedTokens []string
        for _, token := range tokens {
            stemmedTokens = append(stemmedTokens, stemmer.Stem(token))
        }
        return stemmedTokens
    }

```

4. Analisa Sentimen
Analisis sentimen adalah proses menganalisis teks digital untuk menentukan apakah nada emosional pesan tersebut positif, negatif, atau netral.

Karena keterbatasan Sumber yang dimiliki, untuk Analisa Sentimen ini hanya menggunakan logic sederhana berdasarkan positiveWords dan negativeWords yang terbaca. Semakin lengkap kedua data tersebut. Semakin Relevan Hasil yang di dapatkan

```go
    import (
        "fmt"
        "net/http"
        "strings"

        "github.com/RadhiFadlillah/go-sastrawi"
        "github.com/gin-gonic/gin"
    )

    // Sentimen Handler
    sentiment := analyzeSentiment(stemmedTokens)

    // Sentimen Func
    func analyzeSentiment(tokens []string) string {
        // Positive Words
        positiveWords := []string{
            "trims", "udah", "gan", "makasih", "terima", "kasih", "ternyata", "mantap", "mudah2an",
            "awet", "bagus", "cepat", "ok", "barang", "oke", "sesuai", "paket", "rapi", "mantap", "puas",
            "satisfying", "top", "super", "josss", "pokok", "bisa", "buat", "langganan", "berfungsi", "baik",
            "normal",
        }

        // Negative Words
        negativeWords := []string{
            "sedikit", "kecewa", "lama", "sayang", "mengecewakan", "kecil", "banget", "gagal", "tidak",
            "recommended", "ga", "cocok", "rusak", "kecewa", "tidak", "sesuai", "kotor", "kurang", "rapih",
            "tidak", "baik", "lama", "tidak", "sesuai", "ga", "cocok", "rusak", "kecewa", "rusak", "tidak",
            "sesuai", "tidak", "baik", "kecewa", "buruk", "lama", "lagi", "tidak", "baik", "kecewa", "buruk",
            "tidak", "baik", "rusak", "lama", "buruk", "kecewa", "rusak", "buruk", "tidak", "baik", "kecewa",
            "buruk", "lama", "lagi", "baik",
        }

        positiveCount := 0
        negativeCount := 0

        for _, token := range tokens {
            if contains(positiveWords, token) {
                positiveCount++
            } else if contains(negativeWords, token) {
                negativeCount++
            }
        }

        // Penentuan sentimen berdasarkan jumlah kata positif dan negatif
        if positiveCount > negativeCount {
            return "positif"
        } else if negativeCount > positiveCount {
            return "negatif"
        } else {
            return "netral"
        }
    }


```

## Licence
This project is licensed under the MIT License - see the LICENSE file for details.


