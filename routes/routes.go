package routes

import (
	"simple-sentiment-app/golang/module/sentimen"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"github.com/gin-contrib/cors"
	// swagger embed files
	// gin-swagger middleware
)

func SetupRouter(db *gorm.DB) *gin.Engine {
	r := gin.Default()

	// set db to gin context
	r.Use(func(c *gin.Context) {
		c.Set("db", db)
	})

	// Menambahkan middleware CORS
	configcors := cors.DefaultConfig()
	configcors.AllowOrigins = []string{"*"}                                       // Atur sumber yang diizinkan, bisa diganti dengan daftar origin yang sesuai kebutuhan
	configcors.AllowMethods = []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"} // Atur metode HTTP yang diizinkan
	configcors.AllowHeaders = []string{"Accept", "Authorization", "Content-Type", "Content-Length", "X-CSRF-Token", "Origin", "Host", "Connection", "Accept-Encoding", "Accept-Language", "X-Requested-With"}

	r.Use(cors.New(configcors))

	r.POST("/sentimen", sentimen.AnalyzeSentimentHandler)

	return r
}
