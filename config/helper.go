package config

import (
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

// Helper function untuk mengonversi string menjadi integer
func StrToInt(str string, defaultVal int) int {
	i, err := strconv.Atoi(str)
	if err != nil {
		return defaultVal // Nilai default jika terjadi kesalahan
	}
	return i
}

// SuccessResponse menghasilkan respons API yang berhasil
func SuccessResponse(c *gin.Context, data interface{}, message string) {
	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"data":    data,
		"message": message,
	})
}

func SuccessResponseWithPagination(c *gin.Context, data interface{}, page int, total_page int, total_data int64, message string) {
	c.JSON(http.StatusOK, gin.H{
		"success":      true,
		"data":         data,
		"message":      message,
		"current_page": page,
		"total_page":   total_page,
		"total":        total_data,
	})
}

// ErrorResponse menghasilkan respons API yang mengalami error
func ErrorResponse(c *gin.Context, code int, message string, data ...interface{}) {
	c.JSON(code, gin.H{
		"success": false,
		"data":    data,
		"message": message,
	})
}

// Helper function Menyederhanakan Pagination
func PaginationData(pageInt int, limitInt int, totalData int64) (int, int) {

	offset := (pageInt - 1) * limitInt

	totalPages := int(totalData) / limitInt
	if int(totalData)%limitInt != 0 {
		totalPages++
	}

	return offset, totalPages

}

func FormatTanggalIndonesia(t time.Time) string {
	// Mengonversi tanggal ke lokasi waktu Indonesia
	indonesiaLocation, err := time.LoadLocation("Asia/Jakarta")
	if err != nil {
		return "Error"
	}

	// Mengatur lokasi waktu ke Indonesia
	t = t.In(indonesiaLocation)

	// Array nama bulan dalam bahasa Indonesia
	bulanIndonesia := []string{"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"}

	// Mengambil indeks bulan dari tanggal
	// (indeks dimulai dari 0, sedangkan Month() dimulai dari 1)
	indeksBulan := int(t.Month()) - 1

	// Format tanggal sesuai dengan format "02 Januari 2006"
	indonesiaDateFormat := "02 " + bulanIndonesia[indeksBulan] + " 2006"
	formattedDate := t.Format(indonesiaDateFormat)

	return formattedDate
}

func ConvertStringToIntArray(input string) []int {
	// Pisahkan string menjadi potongan berdasarkan koma
	strArray := strings.Split(input, ",")

	// Inisialisasi slice int
	intArray := make([]int, len(strArray))

	// Loop untuk mengonversi setiap potongan menjadi integer
	for i, str := range strArray {
		num, err := strconv.Atoi(strings.TrimSpace(str))
		if err != nil {
			return nil
		}
		intArray[i] = num
	}

	return intArray
}
