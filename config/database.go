package config

import (
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func ConnectDataBase() *gorm.DB {
	username := GoDotEnvVariable("DATABASE_USERNAME")
	password := GoDotEnvVariable("DATABASE_PASSWORD")
	host := "tcp(" + GoDotEnvVariable("DATABASE_HOST") + ":" + GoDotEnvVariable("DATABASE_PORT") + ")"
	database := GoDotEnvVariable("DATABASE_NAME")

	dsn := fmt.Sprintf("%v:%v@%v/%v?charset=utf8mb4&parseTime=True&loc=Local", username, password, host, database)

	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		panic(err.Error())
	}

	// db.AutoMigrate(&models.Movie{}, &models.AgeRatingCategory{})

	return db
}
