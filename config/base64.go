package config

import "encoding/base64"

// Fungsi untuk melakukan Base64 encode
func Base64Encode(input string) string {
	return base64.StdEncoding.EncodeToString([]byte(input))
}

// Fungsi untuk melakukan Base64 decode
func Base64Decode(input string) (string, error) {
	decodedBytes, err := base64.StdEncoding.DecodeString(input)
	if err != nil {
		return "", err
	}
	return string(decodedBytes), nil
}
