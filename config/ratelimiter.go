package config

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"golang.org/x/time/rate"
)

func CreateLimiterMiddleware(rps float64, burst int, message string) gin.HandlerFunc {
	limiter := rate.NewLimiter(rate.Limit(rps), burst)

	return func(c *gin.Context) {
		if limiter.Allow() {
			c.Next()
		} else {
			ErrorResponse(c, http.StatusTooManyRequests, message)
			c.Abort()
		}
	}
}
