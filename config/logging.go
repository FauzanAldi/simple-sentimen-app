package config

import (
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type UsersLog struct {
	// gorm.Model
	ID        uint      `gorm:"primaryKey" json:"id"`
	UserID    *int      `json:"user_id"`
	Path      string    `json:"path"`
	Method    string    `json:"method"`
	IP        string    `json:"ip"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func (UsersLog) TableName() string {
	return "users_operation_log"
}

func SaveLog(c *gin.Context, userID int) {

	webConfigLog := UsersLog{
		Path:   c.Request.URL.Path,
		Method: c.Request.Method,
		IP:     c.ClientIP(),
	}

	if userID != 0 {
		webConfigLog.UserID = &userID
	}

	db := c.MustGet("db").(*gorm.DB)
	if err := db.Table("users_operation_log").Create(&webConfigLog).Error; err != nil {
		fmt.Println("Gagal menyimpan log ke dalam tabel webconfig: ", err)
	}

}
