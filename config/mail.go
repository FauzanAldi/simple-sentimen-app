package config

import (
	"crypto/tls"

	"gopkg.in/gomail.v2"
)

func SendEmail(toEmail, body string, subject string) error {
	// Konfigurasi pengiriman email
	m := gomail.NewMessage()
	m.SetHeader("From", GoDotEnvVariable("MAIL_FROM_ADDRESS")) // Ganti dengan alamat email Anda
	m.SetHeader("To", toEmail)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", body)

	d := gomail.NewDialer(GoDotEnvVariable("MAIL_HOST"), StrToInt(GoDotEnvVariable("MAIL_PORT"), 587), GoDotEnvVariable("MAIL_FROM_ADDRESS"), GoDotEnvVariable("MAIL_PASSWORD")) // Ganti dengan konfigurasi SMTP Anda

	// Nonaktifkan TLS jika diperlukan
	if GoDotEnvVariable("MAIL_ENCRYPTION") != "tls" {
		// d.TLSConfig = nil
		d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	}

	// Kirim email
	if err := d.DialAndSend(m); err != nil {
		return err
	}

	return nil
}

func SendEmailWithAttach(toEmail, body string, subject string, attach string) error {
	// Konfigurasi pengiriman email
	m := gomail.NewMessage()
	m.SetHeader("From", GoDotEnvVariable("MAIL_FROM_ADDRESS")) // Ganti dengan alamat email Anda
	m.SetHeader("To", toEmail)
	m.SetHeader("Subject", subject)

	m.Embed(attach)
	m.SetBody("text/html", body)

	d := gomail.NewDialer(GoDotEnvVariable("MAIL_HOST"), StrToInt(GoDotEnvVariable("MAIL_PORT"), 587), GoDotEnvVariable("MAIL_FROM_ADDRESS"), GoDotEnvVariable("MAIL_PASSWORD")) // Ganti dengan konfigurasi SMTP Anda

	// Nonaktifkan TLS jika diperlukan
	if GoDotEnvVariable("MAIL_ENCRYPTION") != "tls" {
		// d.TLSConfig = nil
		d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	}

	// Kirim email
	if err := d.DialAndSend(m); err != nil {
		return err
	}

	return nil
}
